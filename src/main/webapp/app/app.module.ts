import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { TechTalkSharedModule } from 'app/shared/shared.module';
import { TechTalkCoreModule } from 'app/core/core.module';
import { TechTalkAppRoutingModule } from './app-routing.module';
import { TechTalkHomeModule } from './home/home.module';
import { TechTalkEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { JhiMainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ErrorComponent } from './layouts/error/error.component';

@NgModule({
  imports: [
    BrowserModule,
    TechTalkSharedModule,
    TechTalkCoreModule,
    TechTalkHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    TechTalkEntityModule,
    TechTalkAppRoutingModule
  ],
  declarations: [JhiMainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, FooterComponent],
  bootstrap: [JhiMainComponent]
})
export class TechTalkAppModule {}
