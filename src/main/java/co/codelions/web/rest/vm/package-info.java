/**
 * View Models used by Spring MVC REST controllers.
 */
package co.codelions.web.rest.vm;
